// 1. Створіть об'єкт product з властивостями name, price та discount.
// Додайте метод для виведення повної ціни товару з урахуванням знижки.
// Викличте цей метод та результат виведіть в консоль.

const product = {
    name: 'Чайник',
    netPrice: 1299.99,
    discount: 300,
    getDiscountedPrice() {
        return this.netPrice - this.discount;
    },
}

console.log(`Вартість товару ${product.name} складає ${product.getDiscountedPrice()}`)

/*
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням
 і віком, наприклад "Привіт, мені 30 років".

 Попросіть користувача ввести своє ім'я та вік за допомогою prompt,
 і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
*/

// const userName = prompt(`Введіть своє ім'я:`);
// const userAge = prompt(`Введіть свій вік:`);
//
// const person = {};
// person.name = userName;
// person.age = userAge;
//
// function greetPerson (user) {
//     if (user.name !== null &&
//         user.name !== '' &&
//         isNaN(user.name) &&
//         user.age !== null &&
//         user.age !== '' &&
//         !isNaN(user.age)
//     ) {
//         return alert(`Привіт, ${user.name}. Тобі ${user.age} років.`);
//     } else {
//         console.log('Введено некоректні значення')}
// }
//
// greetPerson(person);


/*
3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта.

Технічні вимоги:
- Написати функцію для рекурсивного повного клонування об'єкта
  (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
- Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.
*/

// const person = {
//     name: "John",
//     surname: "Blackwood",
//     job: "carpenter",
//     family: {
//         count: 3,
//         members: {
//             family1: {
//                 mother: {name: 'Сара'},
//                 father: {name: 'Иван'},
//                 sister: {name: 'Джуана'},
//             },
//             father: {name: 'Иван'},
//             sister: {name: 'Джуана'},
//             previousJobs: [1, 2, 3, [31, 32, 33], 15],
//         },
//     },
//     anotherFamily: {
//         count: 2,
//         members: {
//             family2: {
//                 mother: {name: 'Сара'},
//                 father: {name: 'Иван'},
//                 sister: {name: 'Джуана'},
//             },
//             father: {name: 'Иван'},
//             sister: {name: 'Джуана'},
//             previousJobs: [34, 34, 144, [131, 342, 353], 115],
//         },
//     },
// };
// const person2 = {};
//
//
// function cloneObject(clone, obj) {
//     for (let keys in obj) {
//         if (typeof obj[keys] !== 'object') {
//             clone[keys] = obj[keys];
//         } else {
//             if (!Array.isArray(obj[keys])) {
//                 clone[keys] = Object.create(obj[keys]);
//                 cloneObject(clone[keys], obj[keys]);
//             } else {
//                 clone[keys] = new Array(obj[keys]);
//                 cloneObject(clone[keys], obj[keys]);
//             }
//         }
//     }
// }
//
// cloneObject(person2, person);
//
// person.name = 'Vasya';
// person.family.count = 4;
// person.anotherFamily.count = 10;
// person.family.members.family1.mother.name = 'Kate';
// person.anotherFamily.members.previousJobs = [34343, 343434, 35354];
//
// console.log(person);
// console.log(person2);
//



